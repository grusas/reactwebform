﻿<%@ Page Language="C#" AutoEventWireup="true" CodeBehind="User.aspx.cs" Inherits="ReactWebform.User" %>

<!DOCTYPE html>

<html xmlns="http://www.w3.org/1999/xhtml">

<head runat="server">
    <style>
        html {
            height: 100%
        }

        body {
            align-items: flex-start;
            display: flex;
            flex-direction: column;
            font-family: "HelveticaNeue-Light", "Helvetica Neue Light", "Helvetica Neue", sans-serif;
            height: 100%;
            justify-content: flex-start;
            text-align: left;
        }        

        .input {
            font-size: 1rem;
            margin-bottom: 2rem;
        }

        .button {
            background: #eee;
            border: 1px solid #ddd;
            cursor: pointer;
            margin-left:0.25rem;
            font-size: 1.05rem;
            margin-bottom: 1rem;
        }
    </style>
    <title></title>
</head>
<body>   


    <div id="content">
        <div id="users" runat="server"></div>        
    </div>


    <asp:PlaceHolder runat="server">
        <script src="Scripts/lib/react.js"></script>
        <script src="Scripts/lib/underscore.js"></script>
        <script src="Scripts/contacts.js"></script>
        <script type="text/javascript">
            <asp:Literal ID="litInitJS" runat="server" />
        </script>
    </asp:PlaceHolder>
</body>
</html>
