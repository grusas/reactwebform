﻿<%@ Page Language="C#" AutoEventWireup="true" CodeBehind="Registration.aspx.cs" Inherits="ReactWebform.Registration" %>

<!DOCTYPE html>

<html xmlns="http://www.w3.org/1999/xhtml">
<head runat="server">
    <title></title>
   
<link href="https://maxcdn.bootstrapcdn.com/bootstrap/3.3.6/css/bootstrap.min.css" rel="stylesheet" />

<script src="https://code.jquery.com/jquery-2.2.0.min.js"></script>

<script src="https://cdnjs.cloudflare.com/ajax/libs/react/0.14.6/react.js"></script>
<script src="https://cdnjs.cloudflare.com/ajax/libs/react/0.14.6/react-dom.js"></script>
<script src="https://cdnjs.cloudflare.com/ajax/libs/babel-core/5.8.23/browser.min.js"></script>

<script src="./Scripts/registration.js" type="text/babel"></script>
 
<style type="text/css">
    .form-control.error{
        border-color:red;
        background-color:#FFF6F6;
    }
    span.error{
        color:red;
    }
    .servermessage{
        font-size:32px;
        margin-top:20px;
    }
</style>
</head>
<body>
    <div class="container">
    <h2>Create simple form with validation in React.js</h2>
    <div id="contactFormArea">
 
    </div>
</div>
</body>
</html>
