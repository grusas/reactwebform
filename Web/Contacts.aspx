﻿<%@ Page Language="C#" AutoEventWireup="true" CodeBehind="Contacts.aspx.cs" Inherits="ReactWebform.Contacts" %>

<!DOCTYPE html>

<html xmlns="http://www.w3.org/1999/xhtml">
<head runat="server">
    <title></title>
</head>
<body>
    <div id="content">
        <div id="users" runat="server"></div>        
    </div>

    <asp:PlaceHolder runat="server">
        <script src="Scripts/lib/react.js"></script>
        <script src="Scripts/lib/underscore.js"></script>
        <script src="Scripts/contacts.js"></script>
        <script type="text/javascript">
            <asp:Literal ID="litInitJS" runat="server" />
        </script>
    </asp:PlaceHolder>
</body>
</html>
