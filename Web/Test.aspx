﻿<%@ Page Language="C#" AutoEventWireup="true" CodeBehind="Test.aspx.cs" Inherits="ReactWebform.Test" %>

<!DOCTYPE html>

<html xmlns="http://www.w3.org/1999/xhtml">
<head runat="server">
    <title></title>
    <script type="text/javascript" src="http://ajax.googleapis.com/ajax/libs/jquery/1.4.2/jquery.min.js"></script>
    <script> 
        //GET token
        $(document).ready(GetAuthorization());

        function GetToken() {

            $.ajax({
                type: "GET",
                url: "http://localhost:39048/api/token",
                dataType: "html",
                contentType: "application/x-www-form-urlencoded; charset=UTF-8", // this is the default value, so it's optional                
                //headers: { "Authorization": "Bearer eyJhbGciOiJIUzI1NiIsInR5cCI6IkpXVCJ9.eyJ1bmlxdWVfbmFtZSI6Imd1cyIsIm5iZiI6MTUxMjY4MTA1NCwiZXhwIjoxNTEyNjgyMjU0LCJpYXQiOjE1MTI2ODEwNTR9.IYNvTvSk_xGbyWyUWNU2mE9Nd784agBFamqXXy5325g" },
                //dataType: "text",
                //contentType: "application/json",
                success: function (data) {
                    var token = data;
                    $('<tr><td>' + token + '</td><td>' + '</tr>').appendTo('#token');
                    $('#token').html(token);
                    var storage = window.localStorage;
                    storage.setItem("access_token", token);
                },
                error: function (xhr) {
                    alert(xhr.responseText);
                }
            });
        }

        function ValidateToken() {
            $.ajax({
                url: "http://localhost:39048/api/value",
                type: 'GET',
                // Fetch the stored token from localStorage and set in the header
                //headers: { "Authorization": localStorage.getItem('token') }
                headers: {
                    'Content-Type': 'application/x-www-form-urlencoded', 'Authorization': 'Bearer ' + 'eyJhbGciOiJIUzI1NiIsInR5cCI6IkpXVCJ9.eyJ1bmlxdWVfbmFtZSI6Imd1cyIsIm5iZiI6MTUxMjY4MTA1NCwiZXhwIjoxNTEyNjgyMjU0LCJpYXQiOjE1MTI2ODEwNTR9.IYNvTvSk_xGbyWyUWNU2mE9Nd784agBFamqXXy5325g'
                },
                success: function (data) {
                    var token = data;
                    $('<tr><td>' + token + '</td><td>' + '</tr>').appendTo('#token');
                    $('#token').html(token);
                },
                error: function (xhr) {
                    alert(xhr.responseText);
                }
            });
        }

        function CheckToken() {
            $.ajax({
                url: "http://localhost:39048/api/value",
                type: 'POST',
                data: {},
                //dataType: 'jsonp',
                //beforeSend: function (xhr) {
                //    xhr.setRequestHeader('Authorization', 'bearer t-7614f875-8423-4f20-a674-d7cf3096290e');
                //},
                beforeSend: function (xhr, settings) { xhr.setRequestHeader('Authorization', 'Bearer ' + token); },
                success: function () { },
                error: function () { },
            });
        }

        function GetAuthorization() {
            //var token = 'eyJhbGciOiJIUzI1NiIsInR5cCI6IkpXVCJ9.eyJ1bmlxdWVfbmFtZSI6Imd1cyIsIm5iZiI6MTUxMjc0MTkxMywiZXhwIjoxNTEyNzQzMTEzLCJpYXQiOjE1MTI3NDE5MTN9.e0FKQCF4KRlaUcGWMHjgi5NJalH5CIfvbgfFx9p3xyg';
            var storage = window.localStorage;
            var token = storage.getItem("access_token");
            $.ajax({
                type: "GET",
                url: "http://localhost:39048/api/value",
                data: {},     //Data sent to server
                //contentType: 'application/x-www-form-urlencoded',
                contentType: "application/json",
                dataType: "json",
                beforeSend: function (xhr) {   //Include the bearer token in header
                    xhr.setRequestHeader("Authorization", 'Bearer ' + token);
                },
                success: function (response) {
                    $('#token').html(response.responseText);
                },
                error: function (xhr) {
                    alert(xhr.responseText);
                }               
            });
        }
    </script>

</head>
<body>
    <div class="jumbotron">
        <table border='1' id="token">
            <!-- Make a Header Row -->
            <tr>
                <td><b>Token</b></td>
            </tr>
        </table>
    </div>
</body>
</html>
