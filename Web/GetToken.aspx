﻿<%@ Page Language="C#" AutoEventWireup="true" CodeBehind="GetToken.aspx.cs" Inherits="ReactWebform.GetToken" %>

<!DOCTYPE html>

<html xmlns="http://www.w3.org/1999/xhtml">
<head runat="server">
    <title></title>
    <script type="text/javascript" src="http://ajax.googleapis.com/ajax/libs/jquery/1.4.2/jquery.min.js"></script>
    <script> 
        //GET token
        $(document).ready(GetToken());       

        function GetToken() {
            $.ajax({
                type: "GET",
                url: "http://localhost:39048/api/token",
                dataType: "json",
                //contentType: "application/x-www-form-urlencoded; charset=UTF-8", // this is the default value, so it's optional                
                //headers: { "Authorization": "Bearer eyJhbGciOiJIUzI1NiIsInR5cCI6IkpXVCJ9.eyJ1bmlxdWVfbmFtZSI6Imd1cyIsIm5iZiI6MTUxMjY4MTA1NCwiZXhwIjoxNTEyNjgyMjU0LCJpYXQiOjE1MTI2ODEwNTR9.IYNvTvSk_xGbyWyUWNU2mE9Nd784agBFamqXXy5325g" },
                //dataType: "text",
                contentType: "application/json",
                success: function (data) {
                    var token = data;
                    $('<tr><td>' + token + '</td><td>' + '</tr>').appendTo('#token');
                    $('#token').html(token);
                    var storage = window.localStorage;
                    //var tokenStorage = JSON.stringify(token);
                    storage.setItem("access_token", token);
                },
                error: function (xhr) {
                    alert(xhr.responseText);
                }
            });
        }
      
    </script>

</head>
<body>
    <div class="jumbotron">
        <table border='1' id="token">
            <!-- Make a Header Row -->
            <tr>
                <td><b>Token</b></td>
            </tr>
        </table>
    </div>
</body>
</html>
