﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Net;
using System.Net.Http;
using System.Web.Http;
using System.Web.Http.Results;
using System.Web.Mvc;

namespace ReactWebform.Controllers
{
    public class RegistrationController : ApiController
    {       

        [System.Web.Mvc.HttpPost]
        public JsonResult SaveContactData(ContactsData contact)
        {
            bool status = false;
            string message = "";
            if (ModelState.IsValid)
            {
                using (Database1Entities dc = new Database1Entities())
                {
                   
                    dc.ContactsDatas.Add(contact);
                    dc.SaveChanges();
                    status = true;
                    message = "Thank you for submit your query";
                }
            }
            else
            {
                message = "Failed! Please try again";
            }
            return new JsonResult { Data = new { status = status, message = message } };
        }
    }
}

