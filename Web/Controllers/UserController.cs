﻿using Newtonsoft.Json.Linq;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Net;
using System.Net.Http;
using System.Web.Http;
using ReactWebform.Models;

namespace ReactWebform.Controllers
{
    public class UserController : ApiController
    {

        public dynamic GetUsers()
        {
            return new List<UserModel>()
            {
                new UserModel { Name = "Ana" , Country = "Argentina" },
                new UserModel { Name = "Anthony" , Country = "Chile" },
                new UserModel { Name = "Bob" , Country = "United States" },
                new UserModel { Name = "Charles" , Country = "Mexico" },
            };
        }

        [HttpPost]
        public dynamic Add(dynamic user)
        {
            string json = user.ToString();

            UserModel c = JObject.Parse(json).ToObject<UserModel>();

            IList<UserModel> users = GetUsers();

            users.Add(c);

            return users;
        }


        public IEnumerable<ContactsData> GetUsersData()
        {
            using (Database1Entities dc = new Database1Entities())
            {
                List<ContactsData> contacts = dc.ContactsDatas.ToList();

                List<ContactsData> userList = new List<ContactsData>();
                foreach (ContactsData user in contacts)
                {
                    userList.Add(new ContactsData
                    {                       
                        Fullname = user.Fullname,
                        Email = user.Email           

                    });
                }
                return userList;
            }
        }

    }
}