﻿using React;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;
using ReactWebform.Controllers;

namespace ReactWebform
{
    public partial class User : System.Web.UI.Page
    {
        protected void Page_Load(object sender, EventArgs e)
        {           
            
            var env = AssemblyRegistration.Container.Resolve<IReactEnvironment>();            
           
            var userController = new UserController();
            
            var data = userController.GetUsersData();

            var commentBoxOptions = new
            {
                initialData = data,
                url = "api/User",
                submitUrl = "api/User/add",
                pollInterval = 2000
            };
                       
            var commentBoxComponent = env.CreateComponent("CommentBox", commentBoxOptions);
            users.InnerHtml = commentBoxComponent.RenderHtml();

            string initJS = env.GetInitJavaScript();
            litInitJS.Text = initJS;


        }
    }
}