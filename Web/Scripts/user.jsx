﻿
var CommentBox = React.createClass({    
    handleUserSubmit: function(user) {
        var self = this;        
        var xhr = new XMLHttpRequest();
        xhr.open('post', this.props.submitUrl, true);
        xhr.setRequestHeader("Content-type","text/json");
        xhr.onload = function() {
            var data = JSON.parse(this.responseText);
            //console.log(data);
            self.setState({ data: data });           
        };
        xhr.send(JSON.stringify(user));
    },
    getInitialState: function() {
        return {data: this.props.initialData};
    },
    componentDidMount: function() {
		console.log('mounted');        
    },
    render: function() {
        return (
          <div>
            <h1>Users</h1>
            <UserList data={this.state.data} />
            <UserForm onUserSubmit={this.handleUserSubmit} />
          </div>
      );
    }
});

var UserList = React.createClass({
    render: function() {
        var userNodes = this.props.data.map(function (user, i) {
            return (
                <User key={i} name={user.Name}>
                    {user.Country}
                </User>
                );
        });
        return (
          <div>
            {userNodes}
          </div>
        );
     }
});

var User = React.createClass({
    render: function() {
        return (
          <div >
            <h2 >
              {this.props.name}
            </h2>
        {this.props.children}
        </div>
      );
    }
});

var UserForm = React.createClass({
    handleUSubmit: function(e) {		
        e.preventDefault();
        var name = this.refs.name.getDOMNode().value.trim();
        var country = this.refs.country.getDOMNode().value.trim();
        if (!country || !name) {
            return;
        }
        this.props.onUserSubmit({Name: name, Country: country});
        this.refs.name.getDOMNode().value = '';
        this.refs.country.getDOMNode().value = '';
		return false;
    },
    render: function() {
		console.log('rendering user form');
        return (
            <form onSubmit={this.handleUSubmit}>
                <input className="input" type="text" placeholder="User name" ref="name" />
                <input className="input" type="text" placeholder="User country" ref="country" />
                <input className="button" type="submit" value="Post" />
			</form>
		);
	}
});



