﻿
var ContactBox = React.createClass({    
    getInitialState: function() {
        return {data: this.props.initialData};
    },
    componentDidMount: function() {
		console.log('mounted');        
    },
    render: function() {
        return (
          <div>
            <h1>Contacts</h1>
            <UserList data={this.state.data} />
            <UserForm onUserSubmit={this.handleUserSubmit} />
          </div>
      );
    }
});

var UserList = React.createClass({
    render: function() {
        var userNodes = this.props.data.map(function (user, i) {
            return (
                <User key={i} name={user.Fullname}>
                    {user.Email}
                </User>
                );
        });
        return (
          <div>
            {userNodes}
          </div>
        );
     }
});

var User = React.createClass({
    render: function() {
        return (
          <div >
            <h2 >
              {this.props.name}
            </h2>
        {this.props.children}
        </div>
      );
    }
});

var UserForm = React.createClass({
    handleUSubmit: function(e) {		
        e.preventDefault();
        var name = this.refs.name.getDOMNode().value.trim();
        var country = this.refs.country.getDOMNode().value.trim();
        if (!country || !name) {
            return;
        }
        this.props.onUserSubmit({ Fullname: name, Email: country});
        this.refs.name.getDOMNode().value = '';
        this.refs.country.getDOMNode().value = '';
		return false;
    },
    render: function() {
		console.log('rendering user form');
        return (
            <form onSubmit={this.handleUSubmit}>
                <input className="input" type="text" placeholder="User name" ref="name" />
                <input className="input" type="text" placeholder="User country" ref="country" />
                <input className="button" type="submit" value="Post" />
			</form>
		);
	}
});



