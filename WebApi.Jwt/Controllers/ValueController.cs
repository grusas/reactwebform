﻿using System.Web.Http;
using System.Web.Http.Cors;
using WebApi.Jwt.Filters;

namespace WebApi.Jwt.Controllers
{
    [EnableCors(origins: "http://localhost:11974", headers: "*", methods: "*")]
    public class ValueController : ApiController
    {
        [JwtAuthentication]
        public string Get()
        {
            return "value";
        }
    }
}
