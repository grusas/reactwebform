﻿using System.Net;
using System.Web.Http;
using System.Web.Http.Cors;

namespace WebApi.Jwt.Controllers
{
    [EnableCors(origins: "http://localhost:11974", headers: "*", methods: "*")]
    public class TokenController : ApiController
    {
        // THis is naive endpoint for demo, it should use Basic authentication to provide token or POST request
        [AllowAnonymous]
        public string Get(string username, string password)
        {
            if (CheckUser(username, password))
            {
                return JwtManager.GenerateToken(username);
            }

            throw new HttpResponseException(HttpStatusCode.Unauthorized);
        }

        // THis is naive endpoint for demo, it should use Basic authentication to provide token or POST request
        [AllowAnonymous]
        
        public string Get()
        {
            return JwtManager.GenerateToken("gus");
            
            throw new HttpResponseException(HttpStatusCode.Unauthorized);
        }

        public bool CheckUser(string username, string password)
        {
            // should check in the database
            return true;
        }
    }
}
